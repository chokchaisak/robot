curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt install nodejs
cd robotex/
npm install -g @angular/cli
npm i
sh app-build.js
cd robotex-backend/
docker pull 30010123/robottest:v1
docker run -d --restart always --name robot-ex -p 80:80 -p 4200:4200 30010123/robot-ex:v1
sudo apt install openjdk-8-jdk
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
