import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  constructor(
    public apiService: ApiService,
    private router: Router
    )
     {

     }
  clickBackDashboard(): void {
    this.router.navigateByUrl('dashboard');
  }

  ngOnInit(): void {}
}
