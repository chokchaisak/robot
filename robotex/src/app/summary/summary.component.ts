import { Component, OnInit } from '@angular/core';
import { ApiService } from "../services/api.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})

export class SummaryComponent implements OnInit {
  constructor(
    public apiService: ApiService,
    private router: Router,
  )
  {

  }
  clickBackDashboard(): void {
    this.router.navigateByUrl('dashboard');
  }

  ngOnInit(): void {}

}
