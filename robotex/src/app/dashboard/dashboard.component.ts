import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { FileItem, FileLikeObject, FileUploader } from 'ng2-file-upload';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { AngularFireAuth } from '@angular/fire/auth';

import io from 'socket.io-client';

const hostIP = 'http://35.192.140.231:3000/';
const UploadURLT = hostIP + 'fileuploadT/';
const UploadURLE = hostIP + 'fileuploadE/';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  Filename: any = 'TESTCASE';
  excelFiles: any;
  excelFilesShow: any = [];
  Messages: any;
  OutputFiles: any;
  OutputFilesShow: any = [];
  testFiles: any;
  testFilesShow: any = [];
  typeFile: any;
  productForm: FormGroup;
  product: any = {};
  mystr: any;
  user = JSON.parse(localStorage.getItem('user'));
  userId = this.user.email;



  public uploaderT: FileUploader = new FileUploader({
    url: UploadURLT + this.userId,
    removeAfterUpload: false,
    autoUpload: true,
    itemAlias: 'files',
  });
  public uploaderE: FileUploader = new FileUploader({
    url: UploadURLE + this.userId,
    removeAfterUpload: false,
    autoUpload: true,
    itemAlias: 'files',
  });

  constructor(
    public apiService: ApiService,
    private router: Router,
    private fb: FormBuilder,
    public afAuth: AngularFireAuth
  ) {
    //add PANGPOND
    console.log("Version 1.0.24")
    this.apiService.onCreateUser().subscribe();
    this.createForm();
  }

  clickViewUser(): void {
    this.router.navigateByUrl('user');
  }

  SignOut() {
    return this.afAuth.auth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['sign-in']);
    });
  }

  createUsr() {
    this.apiService.onCreateUser();
  }

  createForm() {
    this.productForm = this.fb.group({
      text: ['', Validators.required],
    });
  }

  viewId(): void {
    document.getElementById('run').style.display = 'none';
    document.getElementById('save').style.display = 'none';
    document.getElementById('remove').style.display = 'none';
    document.getElementById('edit').style.display = 'none';
    document.getElementById('editview').style.display = 'none';
    document.getElementById('fileuploadT').style.display = 'none';
    document.getElementById('fileuploadE').style.display = 'none';
  }

  clickEditTestCase(item): void {
    document.getElementById('editview').innerHTML = '';
    this.viewId();
    this.apiService.getTestCaseDetail(item).subscribe((data) => {
      this.Messages = data;
      this.Filename = item;
      this.typeFile = 'TESTCASE';
      document.getElementById('view').style.display = 'none';
      document.getElementById('editview').innerHTML = data;
      document.getElementById('editview').style.display = '';
      document.getElementById('save').style.display = '';
    });
  }

  clickSave(item1, item2, item3) {
    if(confirm("Are you sure to Save "+ item2 )) {
      this.mystr = '';
    this.mystr = item3
      .replace(/\n/g, '`')
      .replace(/\r/g, '`')
      .replace(/\//g, '^');
    this.apiService.updateFile(item1, item2, this.mystr).subscribe();
    alert('Update file :' + item2);
    this.clickViewTestCase(item2);
    this.ngOnInit();
      console.log(" Save Success");
    }else{
      alert('The file is not updated :' + item2);
      console.log("The file is not updated");
    }
  }

  clickViewTestCase(item): void {
    this.viewId();
    this.apiService.getTestCaseDetail(item).subscribe((data) => {
      this.Messages = data;
      this.Filename = item;
      this.typeFile = 'TESTCASE';
      document.getElementById('view').style.display = '';
      document.getElementById('view').innerHTML =
        '<pre><code>' + data + '</code></pre>';
      document.getElementById('run').style.display = '';
      document.getElementById('edit').style.display = '';
      document.getElementById('remove').style.display = '';
    });
  }

  clickViewExcel(item): void {
    this.viewId();
    this.apiService.getDataFile(item).subscribe((data) => {
      this.Messages = data;
      this.Filename = item;
      this.typeFile = 'DATA';
      document.getElementById('view').innerHTML = data;
      document.getElementById('save').style.display = '';
      document.getElementById('remove').style.display = '';
    });
  }

  clickViewOutput(item): void {
    this.viewId();
    this.typeFile = 'OUTPUT';
    document.getElementById('view').innerHTML =
      '<iframe frameborder="0" style="width: 100%;height: 100vh;" src="' +
      this.apiService.getOutputView(item) +
      '"></iframe>';
  }

  clickViewSummary(): void {
    this.apiService.viewOutputSummary().subscribe((data) => {
      console.log(data);
    });
    this.router.navigateByUrl('summary');
  }

  viewInputUploadT(): void {
    this.viewId();
    document.getElementById('fileuploadT').style.display = '';
    document.getElementById('view').style.display = 'none';

  }

  viewInputUploadE(): void {
    this.viewId();
    document.getElementById('fileuploadE').style.display = '';

  }

// เช็คไฟล์
  validateFile(checkfile: String) {
    var ext = checkfile.substring(checkfile.lastIndexOf('.') + 1);
    var ext2 = checkfile.charAt(checkfile.lastIndexOf(' '));
    var ext3 = checkfile.substring(checkfile.indexOf('.'));
    console.log (ext3)
    if (ext.toLowerCase() == 'robot') {
      console.log('file robot')
      if(ext2.toLowerCase() != ' '){
        this.uploaderT.onCompleteItem = (
          item: any,
          response: any,
          status: any,
          headers: any
        ) => {
          console.log('FileUpload: uploaded: ', item, status, response);
          this.reloadView();
          this.ngOnInit();
      }
        alert('Upload Successfuly');
        return true;
      }
      console.log('The file name should not contain spaces')
      alert('The file name should not contain spaces');
      return false;
    }
    else {
      console.log('Selected file format is not supported');
      alert('Selected file format is not supported');
        return false;
    }
}


  uploadFileT(): void {
    this.uploaderT.onAfterAddingFile = (file) => {
     var checkfile = file._file.name
      if (!this.validateFile(checkfile)) {
      return false;
    }
     file.withCredentials = false;
     this.reloadView();

    };
  }

  uploadFileE(): void {
    this.uploaderE.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };
    this.uploaderE.onCompleteItem = (
      item: any,
      response: any,
      status: any,
      headers: any
    ) => {
      console.log('FileUpload:uploaded:', item, status, response);
      alert('Upload Successfuly');
      this.reloadTestView();
    };
  }

  reloadView(): void {
    this.apiService.getExcelAllFiles().subscribe((data) => {
      this.excelFilesShow = [];
      this.excelFiles = data
        .replace(/"/g, '')
        .replace('[', '')
        .replace(']', '')
        .split(',');
      for (let index = 0; index < this.excelFiles.length; index++) {
        let data = {
          file: this.excelFiles[index],
          name: this.excelFiles[index],
        };
        if (data.name.length > 13) {
          data.name = this.excelFiles[index].substring(0, 13) + '...';
        }
        this.excelFilesShow.push(data);
      }
      console.log(this.excelFilesShow);
    });
  }

  reloadTestView(): void {
    this.apiService.getAllTestCase().subscribe((data) => {
      this.testFilesShow = [];
      this.testFiles = data
        .replace(/"/g, '')
        .replace('[', '')
        .replace(']', '')
        .split(',');
      for (let index = 0; index < this.testFiles.length; index++) {
        let data = {
          file: this.testFiles[index],
          name: this.testFiles[index],
        };
        if (data.name.length > 13) {
          data.name = this.testFiles[index].substring(0, 13) + '...';
        }
        this.testFilesShow.push(data);
      }
      console.log(this.testFilesShow);
    });
  }

  reloadOutput(): void {
    this.apiService.getOutputAllFiles().subscribe((data) => {
      this.OutputFilesShow = [];
      this.OutputFiles = data
        .replace(/"/g, '')
        .replace('[', '')
        .replace(']', '')
        .split(',');
      for (let index = 0; index < this.OutputFiles.length; index++) {
        let data = {
          file: this.OutputFiles[index],
          name: this.OutputFiles[index],
        };
        if (data.name.length > 13) {
          data.name = this.OutputFiles[index].substring(0, 13) + '...';
        }
        this.OutputFilesShow.push(data);
      }
      console.log(this.OutputFilesShow);
    });
  }

  removeFileData(item1, item2): void {
    if(confirm("Are you sure to remove "+item2)) {
    this.apiService.deleteFile(item1, item2).subscribe();
    alert('Delete file :' + item2);
    document.getElementById('view').style.display = 'none';
    this.ngOnInit();
    this.reloadView();
    window.location.reload()
    }

  }

  executTest(item): void {
    if(confirm("Are you sure to Execute Test "+item)) {
      // alert('Execute Test');
      document.getElementById("view").innerHTML = ''
      const socket = io('http://35.192.140.231:3000');
      console.log(socket)
      socket.emit('data2', {userId : this.user.email, testFile: item});
      socket.on('data2', (res: any) => {
        console.log(res);
        document.getElementById("view").innerText = res
        if (res.includes("Report:")) {
          socket.disconnect()
          this.reloadOutput();
        }
      })
         // this.apiService.executetest(item).subscribe((data) => {
        // document.getElementById('view').innerHTML =
        //   '<pre>' +
        //   data
        //     .replace(/"/g, '')
        //     .replace(/\\n/g, '<br/>')
        //     .replace(/\\r/g, '<br/>') +
        //   '</pre>';
        // alert('Executed');

      // });
    }
  }

  ngOnInit(): void {
    this.viewId();
    this.uploadFileT();
    this.uploadFileE();
    this.reloadView();
    this.reloadOutput();
    this.reloadTestView();
  }
}
