import { Injectable, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { User } from '../services/user';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  userData: any;
  face: any;
  // hostIPA = 'localhost';
  hostIPA = '35.192.140.231';
  httplocal = 'http://' + this.hostIPA;
  port = '3000';
  constructor(
    private httpClient: HttpClient,
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    public router: Router,
    public ngZone: NgZone
  ){
    console.log("Version 1.0.24")
    this.afAuth.authState.subscribe((user) => {
      if (user) {
        this.userData = user;
        console.log(this.userData);

        if(this.userData.emailVerified == false){
          console.log('This email needs to verify before login')
          alert('This email needs to verify before login');
          this.SendVerificationMail()
          this.router.navigate(['verify-email-address']);
        }else{
          localStorage.setItem('user', JSON.stringify(this.userData));
          JSON.parse(localStorage.getItem('user'));
        }
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    });
  }


  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user !== null && user.emailVerified !== false ? true : false;
  }

  onCreateUser() {
    const user = JSON.parse(localStorage.getItem('user'));
    this.face = user.email;
    console.log(this.httplocal + ':' + this.port + '/users/' + this.face);
    return this.httpClient.get(
      this.httplocal + ':' + this.port + '/users/' + this.face,
      { responseType: 'text' }
    );
  }

  getTestCaseDetail(item) {
    return this.httpClient.get(
      this.httplocal +
        ':' +
        this.port +
        '/testcase/' +
        this.face +
        '/file/' +
        item,
      { responseType: 'text' }
    );
  }

  executetest(item) {
    return this.httpClient.get(
      this.httplocal +
        ':' +
        this.port +
        '/testid/' +
        this.face +
        '/command/' +
        item,
      { responseType: 'text' }
    );
  }

  getTest() {
    return this.httpClient.get(
      this.httplocal + ':' + this.port + '/users/' + this.face,
      { responseType: 'text' }
    );
  }

  getAllTestCase() {
    return this.httpClient.get(
      this.httplocal + ':' + this.port + '/testcase/' + this.face,
      { responseType: 'text' }
    );
  }

  deleteFile(item1, item2) {
    return this.httpClient.get(
      this.httplocal +
        ':' +
        this.port +
        '/remove/' +
        this.face +
        '/type/' +
        item1 +
        '/name/' +
        item2,
      { responseType: 'text' }
    );
  }

  getOutputFile(item) {
    return this.httpClient.get(
      this.httplocal +
        ':' +
        this.port +
        '/output/' +
        this.face +
        '/file/' +
        item,
      { responseType: 'text' }
    );
  }

  updateFile(item1, item2, item3) {
    return this.httpClient.get(
      this.httplocal +
        ':' +
        this.port +
        '/update/' +
        this.face +
        '/type/' +
        item1 +
        '/name/' +
        item2 +
        '/content/' +
        item3,
      { responseType: 'text' }
    );
  }

  getExcelAllFiles() {
    return this.httpClient.get(
      this.httplocal + ':' + this.port + '/data/' + this.face,
      { responseType: 'text' }
    );
  }

  getDataFile(item) {
    return this.httpClient.get(
      this.httplocal + ':' + this.port + '/data/' + this.face + '/file/' + item,
      { responseType: 'text' }
    );
  }

  getOutputAllFiles() {
    return this.httpClient.get(
      this.httplocal + ':' + this.port + '/output/' + this.face,
      { responseType: 'text' }
    );
  }

  getOutputView(item) {
    const urlView =
      this.httplocal + ':' + this.port + '/view/' + this.face + '/file/' + item;
    return urlView;
  }

  viewOutputSummary() {
    console.log(this.httplocal + ':' + this.port + '/summary/' + this.face );
    return this.httpClient.get(
      this.httplocal + ':' + this.port + '/summary/' + this.face ,
      { responseType: 'text' }
    );
  }

  SignIn(email, password) {
    if(this.afAuth.auth){
      return this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then((result) => {
        this.ngZone.run(() => {
          this.router.navigate(['dashboard']);
        });
        this.SetUserData(result.user);
      })
      .catch((error) => {
        window.alert(error.message)
      });
    }
  }

  SignUp(email, password) {
    const Format_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var ext = password.charAt(password.lastIndexOf(' '));
    if(email.match(Format_email)){
        if(ext.toLowerCase() != ' '){
          return this.afAuth.auth
          .createUserWithEmailAndPassword(email, password)
          .then((result) => {
            this.SendVerificationMail();
            this.SetUserData(result.user);
          })
          .catch((error) => {
            window.alert(error.message);
          });
        }else{
          console.log('The password should not contain spaces')
          alert('The password should not contain spaces');
        }
    }else{
      console.log('The email address is badly formatted.')
      alert('The email address is badly formatted.');
    }
  }

  SendVerificationMail() {
    return this.afAuth.auth.currentUser
    .sendEmailVerification()
    .then(() => {
      this.router.navigate(['verify-email-address']);
    });
  }

  ForgotPassword(passwordResetEmail) {
    return this.afAuth.auth
      .sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        window.alert('Your link to reset password sent to your email, Please check your inbox');
        this.router.navigate(['sign-in']);
      })
      .catch((error) => {
        window.alert(error);
      });

  }

  GoogleAuth() {
    return this.AuthLogin(new auth.GoogleAuthProvider());
  }

  FacebookAuth() {
    return this.AuthLogin(new auth.FacebookAuthProvider());
  }

  GithubAuth() {
    return this.AuthLogin(new auth.GithubAuthProvider());
  }

  AuthLogin(provider) {
    return this.afAuth.auth
      .signInWithPopup(provider)
      .then((result) => {
        this.ngZone.run(() => {
          this.router.navigate(['dashboard']);
        });
        this.SetUserData(result.user);
      })
      .catch((error) => {
        window.alert(error);
      });
  }

  SetUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(
      `users/${user.uid}`
    );
    const userData: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
    };
    return userRef.set(userData, {
      merge: true,
    });
  }

  SignOut() {
    return this.afAuth.auth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['sign-in']);
    });
  }
}
