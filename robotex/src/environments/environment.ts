// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig:{
    apiKey: "AIzaSyDMBF7dTPR6FmdRvlJw71FevGeJJw59EsA",
    authDomain: "robot-ex-ce7c2.firebaseapp.com",
    databaseURL: "https://robot-ex-ce7c2.firebaseio.com",
    projectId: "robot-ex",
    storageBucket: "robot-ex.appspot.com",
    messagingSenderId: "672918031117",
    appId: "1:672918031117:web:e0b4d4d290fd7e095abe44",
    measurementId: "G-QC3VH3QZ9S"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
