var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { createBuilder } from "@angular-devkit/architect";
import { NodeJsSyncHost } from "@angular-devkit/core/node";
import deploy from "./actions";
import { experimental, join, normalize } from "@angular-devkit/core";
import { getFirebaseProjectName } from "../utils";
export default createBuilder((_, context) => __awaiter(this, void 0, void 0, function* () {
    const root = normalize(context.workspaceRoot);
    const workspace = new experimental.workspace.Workspace(root, new NodeJsSyncHost());
    yield workspace
        .loadWorkspaceFromHost(normalize("angular.json"))
        .toPromise();
    if (!context.target) {
        throw new Error("Cannot deploy the application without a target");
    }
    const project = workspace.getProject(context.target.project);
    const firebaseProject = getFirebaseProjectName(workspace.root, context.target.project);
    try {
        yield deploy(require("firebase-tools"), context, join(workspace.root, project.root), firebaseProject);
    }
    catch (e) {
        console.error("Error when trying to deploy: ");
        console.error(e.message);
        return { success: false };
    }
    return { success: true };
}));
//# sourceMappingURL=builder.js.map