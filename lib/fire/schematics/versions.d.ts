export declare const dependencies: {
    "firebase": string;
};
export declare const devDependencies: {
    "@angular-devkit/architect": string;
    "firebase-tools": string;
    "fuzzy": string;
    "inquirer": string;
    "inquirer-autocomplete-prompt": string;
};
