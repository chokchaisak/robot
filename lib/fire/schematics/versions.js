"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dependencies = {
    "firebase": ">= 5.5.7 <7"
};
exports.devDependencies = {
    "@angular-devkit/architect": "<0.900 || ^0.900.0-0 || ^9.0.0-0",
    "firebase-tools": "^6.10.0",
    "fuzzy": "^0.1.3",
    "inquirer": "^6.2.2",
    "inquirer-autocomplete-prompt": "^1.0.1"
};
//# sourceMappingURL=versions.js.map