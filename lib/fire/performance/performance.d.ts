import { NgZone, ApplicationRef, InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { performance } from 'firebase/app';
import { FirebaseApp } from '@angular/fire';
import * as ɵngcc0 from '@angular/core';
export declare const AUTOMATICALLY_TRACE_CORE_NG_METRICS: InjectionToken<boolean>;
export declare const INSTRUMENTATION_ENABLED: InjectionToken<boolean>;
export declare const DATA_COLLECTION_ENABLED: InjectionToken<boolean>;
export declare type TraceOptions = {
    metrics?: {
        [key: string]: number;
    };
    attributes?: {
        [key: string]: string;
    };
    attribute$?: {
        [key: string]: Observable<string>;
    };
    incrementMetric$?: {
        [key: string]: Observable<number | void | null | undefined>;
    };
    metric$?: {
        [key: string]: Observable<number>;
    };
};
export declare class AngularFirePerformance {
    private zone;
    performance: Observable<performance.Performance>;
    constructor(app: FirebaseApp, automaticallyTraceCoreNgMetrics: boolean | null, instrumentationEnabled: boolean | null, dataCollectionEnabled: boolean | null, appRef: ApplicationRef, zone: NgZone);
    trace$: (name: string, options?: TraceOptions | undefined) => Observable<void>;
    traceUntil: <T = any>(name: string, test: (a: T) => boolean, options?: (TraceOptions & {
        orComplete?: boolean | undefined;
    }) | undefined) => (source$: Observable<T>) => Observable<T>;
    traceWhile: <T = any>(name: string, test: (a: T) => boolean, options?: (TraceOptions & {
        orComplete?: boolean | undefined;
    }) | undefined) => (source$: Observable<T>) => Observable<T>;
    traceUntilComplete: <T = any>(name: string, options?: TraceOptions | undefined) => (source$: Observable<T>) => Observable<T>;
    traceUntilFirst: <T = any>(name: string, options?: TraceOptions | undefined) => (source$: Observable<T>) => Observable<T>;
    trace: <T = any>(name: string, options?: TraceOptions | undefined) => (source$: Observable<T>) => Observable<T>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AngularFirePerformance, [null, { optional: true; }, { optional: true; }, { optional: true; }, null, null]>;
    static ɵprov: ɵngcc0.ɵɵInjectableDef<AngularFirePerformance>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVyZm9ybWFuY2UuZC50cyIsInNvdXJjZXMiOlsicGVyZm9ybWFuY2UuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ1pvbmUsIEFwcGxpY2F0aW9uUmVmLCBJbmplY3Rpb25Ub2tlbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgcGVyZm9ybWFuY2UgfSBmcm9tICdmaXJlYmFzZS9hcHAnO1xuaW1wb3J0IHsgRmlyZWJhc2VBcHAgfSBmcm9tICdAYW5ndWxhci9maXJlJztcbmV4cG9ydCBkZWNsYXJlIGNvbnN0IEFVVE9NQVRJQ0FMTFlfVFJBQ0VfQ09SRV9OR19NRVRSSUNTOiBJbmplY3Rpb25Ub2tlbjxib29sZWFuPjtcbmV4cG9ydCBkZWNsYXJlIGNvbnN0IElOU1RSVU1FTlRBVElPTl9FTkFCTEVEOiBJbmplY3Rpb25Ub2tlbjxib29sZWFuPjtcbmV4cG9ydCBkZWNsYXJlIGNvbnN0IERBVEFfQ09MTEVDVElPTl9FTkFCTEVEOiBJbmplY3Rpb25Ub2tlbjxib29sZWFuPjtcbmV4cG9ydCBkZWNsYXJlIHR5cGUgVHJhY2VPcHRpb25zID0ge1xuICAgIG1ldHJpY3M/OiB7XG4gICAgICAgIFtrZXk6IHN0cmluZ106IG51bWJlcjtcbiAgICB9O1xuICAgIGF0dHJpYnV0ZXM/OiB7XG4gICAgICAgIFtrZXk6IHN0cmluZ106IHN0cmluZztcbiAgICB9O1xuICAgIGF0dHJpYnV0ZSQ/OiB7XG4gICAgICAgIFtrZXk6IHN0cmluZ106IE9ic2VydmFibGU8c3RyaW5nPjtcbiAgICB9O1xuICAgIGluY3JlbWVudE1ldHJpYyQ/OiB7XG4gICAgICAgIFtrZXk6IHN0cmluZ106IE9ic2VydmFibGU8bnVtYmVyIHwgdm9pZCB8IG51bGwgfCB1bmRlZmluZWQ+O1xuICAgIH07XG4gICAgbWV0cmljJD86IHtcbiAgICAgICAgW2tleTogc3RyaW5nXTogT2JzZXJ2YWJsZTxudW1iZXI+O1xuICAgIH07XG59O1xuZXhwb3J0IGRlY2xhcmUgY2xhc3MgQW5ndWxhckZpcmVQZXJmb3JtYW5jZSB7XG4gICAgcHJpdmF0ZSB6b25lO1xuICAgIHBlcmZvcm1hbmNlOiBPYnNlcnZhYmxlPHBlcmZvcm1hbmNlLlBlcmZvcm1hbmNlPjtcbiAgICBjb25zdHJ1Y3RvcihhcHA6IEZpcmViYXNlQXBwLCBhdXRvbWF0aWNhbGx5VHJhY2VDb3JlTmdNZXRyaWNzOiBib29sZWFuIHwgbnVsbCwgaW5zdHJ1bWVudGF0aW9uRW5hYmxlZDogYm9vbGVhbiB8IG51bGwsIGRhdGFDb2xsZWN0aW9uRW5hYmxlZDogYm9vbGVhbiB8IG51bGwsIGFwcFJlZjogQXBwbGljYXRpb25SZWYsIHpvbmU6IE5nWm9uZSk7XG4gICAgdHJhY2UkOiAobmFtZTogc3RyaW5nLCBvcHRpb25zPzogVHJhY2VPcHRpb25zIHwgdW5kZWZpbmVkKSA9PiBPYnNlcnZhYmxlPHZvaWQ+O1xuICAgIHRyYWNlVW50aWw6IDxUID0gYW55PihuYW1lOiBzdHJpbmcsIHRlc3Q6IChhOiBUKSA9PiBib29sZWFuLCBvcHRpb25zPzogKFRyYWNlT3B0aW9ucyAmIHtcbiAgICAgICAgb3JDb21wbGV0ZT86IGJvb2xlYW4gfCB1bmRlZmluZWQ7XG4gICAgfSkgfCB1bmRlZmluZWQpID0+IChzb3VyY2UkOiBPYnNlcnZhYmxlPFQ+KSA9PiBPYnNlcnZhYmxlPFQ+O1xuICAgIHRyYWNlV2hpbGU6IDxUID0gYW55PihuYW1lOiBzdHJpbmcsIHRlc3Q6IChhOiBUKSA9PiBib29sZWFuLCBvcHRpb25zPzogKFRyYWNlT3B0aW9ucyAmIHtcbiAgICAgICAgb3JDb21wbGV0ZT86IGJvb2xlYW4gfCB1bmRlZmluZWQ7XG4gICAgfSkgfCB1bmRlZmluZWQpID0+IChzb3VyY2UkOiBPYnNlcnZhYmxlPFQ+KSA9PiBPYnNlcnZhYmxlPFQ+O1xuICAgIHRyYWNlVW50aWxDb21wbGV0ZTogPFQgPSBhbnk+KG5hbWU6IHN0cmluZywgb3B0aW9ucz86IFRyYWNlT3B0aW9ucyB8IHVuZGVmaW5lZCkgPT4gKHNvdXJjZSQ6IE9ic2VydmFibGU8VD4pID0+IE9ic2VydmFibGU8VD47XG4gICAgdHJhY2VVbnRpbEZpcnN0OiA8VCA9IGFueT4obmFtZTogc3RyaW5nLCBvcHRpb25zPzogVHJhY2VPcHRpb25zIHwgdW5kZWZpbmVkKSA9PiAoc291cmNlJDogT2JzZXJ2YWJsZTxUPikgPT4gT2JzZXJ2YWJsZTxUPjtcbiAgICB0cmFjZTogPFQgPSBhbnk+KG5hbWU6IHN0cmluZywgb3B0aW9ucz86IFRyYWNlT3B0aW9ucyB8IHVuZGVmaW5lZCkgPT4gKHNvdXJjZSQ6IE9ic2VydmFibGU8VD4pID0+IE9ic2VydmFibGU8VD47XG59XG4iXX0=