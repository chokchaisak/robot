import * as ɵngcc0 from '@angular/core';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { AngularFireStorage } from './storage';
import 'firebase/storage';
let AngularFireStorageModule = class AngularFireStorageModule {
};
AngularFireStorageModule.ɵmod = ɵngcc0.ɵɵdefineNgModule({ type: AngularFireStorageModule });
AngularFireStorageModule.ɵinj = ɵngcc0.ɵɵdefineInjector({ factory: function AngularFireStorageModule_Factory(t) { return new (t || AngularFireStorageModule)(); }, providers: [AngularFireStorage] });
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(AngularFireStorageModule, [{
        type: NgModule,
        args: [{
                providers: [AngularFireStorage]
            }]
    }], null, null); })();
export { AngularFireStorageModule };

//# sourceMappingURL=storage.module.js.map