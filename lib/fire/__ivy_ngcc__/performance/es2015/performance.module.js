import * as ɵngcc0 from '@angular/core';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { NgModule } from '@angular/core';
import { AngularFirePerformance } from './performance';
let AngularFirePerformanceModule = class AngularFirePerformanceModule {
    constructor(_) {
    }
};
AngularFirePerformanceModule.ɵmod = ɵngcc0.ɵɵdefineNgModule({ type: AngularFirePerformanceModule });
AngularFirePerformanceModule.ɵinj = ɵngcc0.ɵɵdefineInjector({ factory: function AngularFirePerformanceModule_Factory(t) { return new (t || AngularFirePerformanceModule)(ɵngcc0.ɵɵinject(AngularFirePerformance)); }, providers: [AngularFirePerformance] });
AngularFirePerformanceModule = __decorate([ __metadata("design:paramtypes", [AngularFirePerformance])
], AngularFirePerformanceModule);
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(AngularFirePerformanceModule, [{
        type: NgModule,
        args: [{
                providers: [AngularFirePerformance]
            }]
    }], function () { return [{ type: AngularFirePerformance }]; }, null); })();
export { AngularFirePerformanceModule };

//# sourceMappingURL=performance.module.js.map