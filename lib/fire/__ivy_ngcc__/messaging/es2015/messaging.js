import * as ɵngcc0 from '@angular/core';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Injectable, Inject, Optional, NgZone, PLATFORM_ID } from '@angular/core';
import { isPlatformServer } from '@angular/common';
import { Observable, from, of, throwError } from 'rxjs';
import { mergeMap, catchError, map, switchMap, concat, defaultIfEmpty } from 'rxjs/operators';
import { runOutsideAngular } from '@angular/fire';
import { FirebaseOptionsToken, FirebaseNameOrConfigToken, _firebaseAppFactory } from '@angular/fire';
let AngularFireMessaging = class AngularFireMessaging {
    constructor(options, nameOrConfig, platformId, zone) {
        const requireMessaging = from(import('firebase/messaging'));
        this.messaging = requireMessaging.pipe(map(() => _firebaseAppFactory(options, nameOrConfig)), map(app => app.messaging()), runOutsideAngular(zone));
        if (!isPlatformServer(platformId)) {
            this.requestPermission = this.messaging.pipe(switchMap(messaging => messaging.requestPermission()), runOutsideAngular(zone));
        }
        else {
            this.requestPermission = throwError('Not available on server platform.');
        }
        this.getToken = this.messaging.pipe(switchMap(messaging => messaging.getToken()), defaultIfEmpty(null), runOutsideAngular(zone));
        const tokenChanges = this.messaging.pipe(switchMap(messaging => new Observable(messaging.onTokenRefresh.bind(messaging)).pipe(switchMap(() => messaging.getToken()))), runOutsideAngular(zone));
        this.tokenChanges = this.getToken.pipe(concat(tokenChanges));
        this.messages = this.messaging.pipe(switchMap(messaging => new Observable(messaging.onMessage.bind(messaging))), runOutsideAngular(zone));
        this.requestToken = this.requestPermission.pipe(catchError(() => of(null)), mergeMap(() => this.tokenChanges));
        this.deleteToken = (token) => this.messaging.pipe(switchMap(messaging => messaging.deleteToken(token)), defaultIfEmpty(false), runOutsideAngular(zone));
    }
};
AngularFireMessaging.ɵfac = function AngularFireMessaging_Factory(t) { return new (t || AngularFireMessaging)(ɵngcc0.ɵɵinject(FirebaseOptionsToken), ɵngcc0.ɵɵinject(FirebaseNameOrConfigToken, 8), ɵngcc0.ɵɵinject(PLATFORM_ID), ɵngcc0.ɵɵinject(ɵngcc0.NgZone)); };
AngularFireMessaging.ɵprov = ɵngcc0.ɵɵdefineInjectable({ token: AngularFireMessaging, factory: AngularFireMessaging.ɵfac });
AngularFireMessaging = __decorate([ __param(0, Inject(FirebaseOptionsToken)),
    __param(1, Optional()), __param(1, Inject(FirebaseNameOrConfigToken)),
    __param(2, Inject(PLATFORM_ID)),
    __metadata("design:paramtypes", [Object, Object, Object,
        NgZone])
], AngularFireMessaging);
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(AngularFireMessaging, [{
        type: Injectable
    }], function () { return [{ type: Object, decorators: [{
                type: Inject,
                args: [FirebaseOptionsToken]
            }] }, { type: Object, decorators: [{
                type: Optional
            }, {
                type: Inject,
                args: [FirebaseNameOrConfigToken]
            }] }, { type: Object, decorators: [{
                type: Inject,
                args: [PLATFORM_ID]
            }] }, { type: ɵngcc0.NgZone }]; }, null); })();
export { AngularFireMessaging };

//# sourceMappingURL=messaging.js.map