import * as ɵngcc0 from '@angular/core';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { AngularFireAuthGuard } from './auth-guard';
let AngularFireAuthGuardModule = class AngularFireAuthGuardModule {
};
AngularFireAuthGuardModule.ɵmod = ɵngcc0.ɵɵdefineNgModule({ type: AngularFireAuthGuardModule });
AngularFireAuthGuardModule.ɵinj = ɵngcc0.ɵɵdefineInjector({ factory: function AngularFireAuthGuardModule_Factory(t) { return new (t || AngularFireAuthGuardModule)(); }, providers: [AngularFireAuthGuard] });
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(AngularFireAuthGuardModule, [{
        type: NgModule,
        args: [{
                providers: [AngularFireAuthGuard]
            }]
    }], null, null); })();
export { AngularFireAuthGuardModule };

//# sourceMappingURL=auth-guard.module.js.map