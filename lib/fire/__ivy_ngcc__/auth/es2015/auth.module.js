import * as ɵngcc0 from '@angular/core';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { AngularFireAuth } from './auth';
import 'firebase/auth';
let AngularFireAuthModule = class AngularFireAuthModule {
};
AngularFireAuthModule.ɵmod = ɵngcc0.ɵɵdefineNgModule({ type: AngularFireAuthModule });
AngularFireAuthModule.ɵinj = ɵngcc0.ɵɵdefineInjector({ factory: function AngularFireAuthModule_Factory(t) { return new (t || AngularFireAuthModule)(); }, providers: [AngularFireAuth] });
/*@__PURE__*/ (function () { ɵngcc0.ɵsetClassMetadata(AngularFireAuthModule, [{
        type: NgModule,
        args: [{
                providers: [AngularFireAuth]
            }]
    }], null, null); })();
export { AngularFireAuthModule };

//# sourceMappingURL=auth.module.js.map