import { NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { FirebaseAppConfig, FirebaseOptions } from '@angular/fire';
import { User, auth } from 'firebase/app';
import { FirebaseAuth } from '@angular/fire';
import * as ɵngcc0 from '@angular/core';
export declare class AngularFireAuth {
    private zone;
    readonly auth: FirebaseAuth;
    readonly authState: Observable<User | null>;
    readonly idToken: Observable<string | null>;
    readonly user: Observable<User | null>;
    readonly idTokenResult: Observable<auth.IdTokenResult | null>;
    constructor(options: FirebaseOptions, nameOrConfig: string | FirebaseAppConfig | null | undefined, platformId: Object, zone: NgZone);
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AngularFireAuth, [null, { optional: true; }, null, null]>;
    static ɵprov: ɵngcc0.ɵɵInjectableDef<AngularFireAuth>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5kLnRzIiwic291cmNlcyI6WyJhdXRoLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nWm9uZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgRmlyZWJhc2VBcHBDb25maWcsIEZpcmViYXNlT3B0aW9ucyB9IGZyb20gJ0Bhbmd1bGFyL2ZpcmUnO1xuaW1wb3J0IHsgVXNlciwgYXV0aCB9IGZyb20gJ2ZpcmViYXNlL2FwcCc7XG5pbXBvcnQgeyBGaXJlYmFzZUF1dGggfSBmcm9tICdAYW5ndWxhci9maXJlJztcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIEFuZ3VsYXJGaXJlQXV0aCB7XG4gICAgcHJpdmF0ZSB6b25lO1xuICAgIHJlYWRvbmx5IGF1dGg6IEZpcmViYXNlQXV0aDtcbiAgICByZWFkb25seSBhdXRoU3RhdGU6IE9ic2VydmFibGU8VXNlciB8IG51bGw+O1xuICAgIHJlYWRvbmx5IGlkVG9rZW46IE9ic2VydmFibGU8c3RyaW5nIHwgbnVsbD47XG4gICAgcmVhZG9ubHkgdXNlcjogT2JzZXJ2YWJsZTxVc2VyIHwgbnVsbD47XG4gICAgcmVhZG9ubHkgaWRUb2tlblJlc3VsdDogT2JzZXJ2YWJsZTxhdXRoLklkVG9rZW5SZXN1bHQgfCBudWxsPjtcbiAgICBjb25zdHJ1Y3RvcihvcHRpb25zOiBGaXJlYmFzZU9wdGlvbnMsIG5hbWVPckNvbmZpZzogc3RyaW5nIHwgRmlyZWJhc2VBcHBDb25maWcgfCBudWxsIHwgdW5kZWZpbmVkLCBwbGF0Zm9ybUlkOiBPYmplY3QsIHpvbmU6IE5nWm9uZSk7XG59XG4iXX0=