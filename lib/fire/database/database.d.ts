import { NgZone } from '@angular/core';
import { PathReference, QueryFn, AngularFireList, AngularFireObject } from './interfaces';
import { FirebaseDatabase, FirebaseOptions, FirebaseAppConfig, RealtimeDatabaseURL, FirebaseZoneScheduler } from '@angular/fire';
import * as ɵngcc0 from '@angular/core';
export declare class AngularFireDatabase {
    readonly database: FirebaseDatabase;
    readonly scheduler: FirebaseZoneScheduler;
    constructor(options: FirebaseOptions, nameOrConfig: string | FirebaseAppConfig | null | undefined, databaseURL: string | null, platformId: Object, zone: NgZone);
    list<T>(pathOrRef: PathReference, queryFn?: QueryFn): AngularFireList<T>;
    object<T>(pathOrRef: PathReference): AngularFireObject<T>;
    createPushId(): string | null;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AngularFireDatabase, [null, { optional: true; }, { optional: true; }, null, null]>;
    static ɵprov: ɵngcc0.ɵɵInjectableDef<AngularFireDatabase>;
}
export { PathReference, DatabaseSnapshot, ChildEvent, ListenEvent, QueryFn, AngularFireList, AngularFireObject, AngularFireAction, Action, SnapshotAction } from './interfaces';
export { RealtimeDatabaseURL };

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YWJhc2UuZC50cyIsInNvdXJjZXMiOlsiZGF0YWJhc2UuZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nWm9uZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUGF0aFJlZmVyZW5jZSwgUXVlcnlGbiwgQW5ndWxhckZpcmVMaXN0LCBBbmd1bGFyRmlyZU9iamVjdCB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5pbXBvcnQgeyBGaXJlYmFzZURhdGFiYXNlLCBGaXJlYmFzZU9wdGlvbnMsIEZpcmViYXNlQXBwQ29uZmlnLCBSZWFsdGltZURhdGFiYXNlVVJMLCBGaXJlYmFzZVpvbmVTY2hlZHVsZXIgfSBmcm9tICdAYW5ndWxhci9maXJlJztcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIEFuZ3VsYXJGaXJlRGF0YWJhc2Uge1xuICAgIHJlYWRvbmx5IGRhdGFiYXNlOiBGaXJlYmFzZURhdGFiYXNlO1xuICAgIHJlYWRvbmx5IHNjaGVkdWxlcjogRmlyZWJhc2Vab25lU2NoZWR1bGVyO1xuICAgIGNvbnN0cnVjdG9yKG9wdGlvbnM6IEZpcmViYXNlT3B0aW9ucywgbmFtZU9yQ29uZmlnOiBzdHJpbmcgfCBGaXJlYmFzZUFwcENvbmZpZyB8IG51bGwgfCB1bmRlZmluZWQsIGRhdGFiYXNlVVJMOiBzdHJpbmcgfCBudWxsLCBwbGF0Zm9ybUlkOiBPYmplY3QsIHpvbmU6IE5nWm9uZSk7XG4gICAgbGlzdDxUPihwYXRoT3JSZWY6IFBhdGhSZWZlcmVuY2UsIHF1ZXJ5Rm4/OiBRdWVyeUZuKTogQW5ndWxhckZpcmVMaXN0PFQ+O1xuICAgIG9iamVjdDxUPihwYXRoT3JSZWY6IFBhdGhSZWZlcmVuY2UpOiBBbmd1bGFyRmlyZU9iamVjdDxUPjtcbiAgICBjcmVhdGVQdXNoSWQoKTogc3RyaW5nIHwgbnVsbDtcbn1cbmV4cG9ydCB7IFBhdGhSZWZlcmVuY2UsIERhdGFiYXNlU25hcHNob3QsIENoaWxkRXZlbnQsIExpc3RlbkV2ZW50LCBRdWVyeUZuLCBBbmd1bGFyRmlyZUxpc3QsIEFuZ3VsYXJGaXJlT2JqZWN0LCBBbmd1bGFyRmlyZUFjdGlvbiwgQWN0aW9uLCBTbmFwc2hvdEFjdGlvbiB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5leHBvcnQgeyBSZWFsdGltZURhdGFiYXNlVVJMIH07XG4iXX0=