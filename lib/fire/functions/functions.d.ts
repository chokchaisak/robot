import { NgZone, InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { FirebaseOptions, FirebaseAppConfig } from '@angular/fire';
import { FirebaseFunctions, FirebaseZoneScheduler } from '@angular/fire';
import * as ɵngcc0 from '@angular/core';
export declare const FunctionsRegionToken: InjectionToken<string>;
export declare const FUNCTIONS_ORIGIN: InjectionToken<string>;
export declare const FUNCTIONS_REGION: InjectionToken<string>;
export declare class AngularFireFunctions {
    readonly functions: FirebaseFunctions;
    readonly scheduler: FirebaseZoneScheduler;
    constructor(options: FirebaseOptions, nameOrConfig: string | FirebaseAppConfig | null | undefined, platformId: Object, zone: NgZone, region: string | null, origin: string | null);
    httpsCallable<T = any, R = any>(name: string): (data: T) => Observable<R>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AngularFireFunctions, [null, { optional: true; }, null, null, { optional: true; }, { optional: true; }]>;
    static ɵprov: ɵngcc0.ɵɵInjectableDef<AngularFireFunctions>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnVuY3Rpb25zLmQudHMiLCJzb3VyY2VzIjpbImZ1bmN0aW9ucy5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdab25lLCBJbmplY3Rpb25Ub2tlbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgRmlyZWJhc2VPcHRpb25zLCBGaXJlYmFzZUFwcENvbmZpZyB9IGZyb20gJ0Bhbmd1bGFyL2ZpcmUnO1xuaW1wb3J0IHsgRmlyZWJhc2VGdW5jdGlvbnMsIEZpcmViYXNlWm9uZVNjaGVkdWxlciB9IGZyb20gJ0Bhbmd1bGFyL2ZpcmUnO1xuZXhwb3J0IGRlY2xhcmUgY29uc3QgRnVuY3Rpb25zUmVnaW9uVG9rZW46IEluamVjdGlvblRva2VuPHN0cmluZz47XG5leHBvcnQgZGVjbGFyZSBjb25zdCBGVU5DVElPTlNfT1JJR0lOOiBJbmplY3Rpb25Ub2tlbjxzdHJpbmc+O1xuZXhwb3J0IGRlY2xhcmUgY29uc3QgRlVOQ1RJT05TX1JFR0lPTjogSW5qZWN0aW9uVG9rZW48c3RyaW5nPjtcbmV4cG9ydCBkZWNsYXJlIGNsYXNzIEFuZ3VsYXJGaXJlRnVuY3Rpb25zIHtcbiAgICByZWFkb25seSBmdW5jdGlvbnM6IEZpcmViYXNlRnVuY3Rpb25zO1xuICAgIHJlYWRvbmx5IHNjaGVkdWxlcjogRmlyZWJhc2Vab25lU2NoZWR1bGVyO1xuICAgIGNvbnN0cnVjdG9yKG9wdGlvbnM6IEZpcmViYXNlT3B0aW9ucywgbmFtZU9yQ29uZmlnOiBzdHJpbmcgfCBGaXJlYmFzZUFwcENvbmZpZyB8IG51bGwgfCB1bmRlZmluZWQsIHBsYXRmb3JtSWQ6IE9iamVjdCwgem9uZTogTmdab25lLCByZWdpb246IHN0cmluZyB8IG51bGwsIG9yaWdpbjogc3RyaW5nIHwgbnVsbCk7XG4gICAgaHR0cHNDYWxsYWJsZTxUID0gYW55LCBSID0gYW55PihuYW1lOiBzdHJpbmcpOiAoZGF0YTogVCkgPT4gT2JzZXJ2YWJsZTxSPjtcbn1cbiJdfQ==