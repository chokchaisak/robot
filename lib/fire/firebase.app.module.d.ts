import { InjectionToken } from '@angular/core';
import { auth, database, firestore, functions, messaging, storage } from 'firebase/app';
import * as ɵngcc0 from '@angular/core';
export declare type FirebaseOptions = {
    [key: string]: any;
};
export declare type FirebaseAppConfig = {
    [key: string]: any;
};
export declare const FirebaseOptionsToken: InjectionToken<FirebaseOptions>;
export declare const FirebaseNameOrConfigToken: InjectionToken<string | FirebaseAppConfig | undefined>;
export declare type FirebaseDatabase = database.Database;
export declare type FirebaseAuth = auth.Auth;
export declare type FirebaseMessaging = messaging.Messaging;
export declare type FirebaseStorage = storage.Storage;
export declare type FirebaseFirestore = firestore.Firestore;
export declare type FirebaseFunctions = functions.Functions;
export declare class FirebaseApp {
    name: string;
    options: {};
    auth: () => FirebaseAuth;
    database: (databaseURL?: string) => FirebaseDatabase;
    messaging: () => FirebaseMessaging;
    performance: () => any;
    storage: (storageBucket?: string) => FirebaseStorage;
    delete: () => Promise<void>;
    firestore: () => FirebaseFirestore;
    functions: (region?: string) => FirebaseFunctions;
}
export declare function _firebaseAppFactory(options: FirebaseOptions, nameOrConfig?: string | FirebaseAppConfig | null): FirebaseApp;
export declare class AngularFireModule {
    static initializeApp(options: FirebaseOptions, nameOrConfig?: string | FirebaseAppConfig): ({
        ngModule: typeof AngularFireModule;
        providers: {
            provide: InjectionToken<string | FirebaseAppConfig | undefined>;
            useValue: string | FirebaseAppConfig | undefined;
        }[];
    })&{ngModule:AngularFireModule};
    static ɵmod: ɵngcc0.ɵɵNgModuleDefWithMeta<AngularFireModule, never, never, never>;
    static ɵinj: ɵngcc0.ɵɵInjectorDef<AngularFireModule>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlyZWJhc2UuYXBwLm1vZHVsZS5kLnRzIiwic291cmNlcyI6WyJmaXJlYmFzZS5hcHAubW9kdWxlLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtGQUErRjs7Ozs7O21DQU0xRjs7O0FBQ0wiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3Rpb25Ub2tlbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgYXV0aCwgZGF0YWJhc2UsIGZpcmVzdG9yZSwgZnVuY3Rpb25zLCBtZXNzYWdpbmcsIHN0b3JhZ2UgfSBmcm9tICdmaXJlYmFzZS9hcHAnO1xuZXhwb3J0IGRlY2xhcmUgdHlwZSBGaXJlYmFzZU9wdGlvbnMgPSB7XG4gICAgW2tleTogc3RyaW5nXTogYW55O1xufTtcbmV4cG9ydCBkZWNsYXJlIHR5cGUgRmlyZWJhc2VBcHBDb25maWcgPSB7XG4gICAgW2tleTogc3RyaW5nXTogYW55O1xufTtcbmV4cG9ydCBkZWNsYXJlIGNvbnN0IEZpcmViYXNlT3B0aW9uc1Rva2VuOiBJbmplY3Rpb25Ub2tlbjxGaXJlYmFzZU9wdGlvbnM+O1xuZXhwb3J0IGRlY2xhcmUgY29uc3QgRmlyZWJhc2VOYW1lT3JDb25maWdUb2tlbjogSW5qZWN0aW9uVG9rZW48c3RyaW5nIHwgRmlyZWJhc2VBcHBDb25maWcgfCB1bmRlZmluZWQ+O1xuZXhwb3J0IGRlY2xhcmUgdHlwZSBGaXJlYmFzZURhdGFiYXNlID0gZGF0YWJhc2UuRGF0YWJhc2U7XG5leHBvcnQgZGVjbGFyZSB0eXBlIEZpcmViYXNlQXV0aCA9IGF1dGguQXV0aDtcbmV4cG9ydCBkZWNsYXJlIHR5cGUgRmlyZWJhc2VNZXNzYWdpbmcgPSBtZXNzYWdpbmcuTWVzc2FnaW5nO1xuZXhwb3J0IGRlY2xhcmUgdHlwZSBGaXJlYmFzZVN0b3JhZ2UgPSBzdG9yYWdlLlN0b3JhZ2U7XG5leHBvcnQgZGVjbGFyZSB0eXBlIEZpcmViYXNlRmlyZXN0b3JlID0gZmlyZXN0b3JlLkZpcmVzdG9yZTtcbmV4cG9ydCBkZWNsYXJlIHR5cGUgRmlyZWJhc2VGdW5jdGlvbnMgPSBmdW5jdGlvbnMuRnVuY3Rpb25zO1xuZXhwb3J0IGRlY2xhcmUgY2xhc3MgRmlyZWJhc2VBcHAge1xuICAgIG5hbWU6IHN0cmluZztcbiAgICBvcHRpb25zOiB7fTtcbiAgICBhdXRoOiAoKSA9PiBGaXJlYmFzZUF1dGg7XG4gICAgZGF0YWJhc2U6IChkYXRhYmFzZVVSTD86IHN0cmluZykgPT4gRmlyZWJhc2VEYXRhYmFzZTtcbiAgICBtZXNzYWdpbmc6ICgpID0+IEZpcmViYXNlTWVzc2FnaW5nO1xuICAgIHBlcmZvcm1hbmNlOiAoKSA9PiBhbnk7XG4gICAgc3RvcmFnZTogKHN0b3JhZ2VCdWNrZXQ/OiBzdHJpbmcpID0+IEZpcmViYXNlU3RvcmFnZTtcbiAgICBkZWxldGU6ICgpID0+IFByb21pc2U8dm9pZD47XG4gICAgZmlyZXN0b3JlOiAoKSA9PiBGaXJlYmFzZUZpcmVzdG9yZTtcbiAgICBmdW5jdGlvbnM6IChyZWdpb24/OiBzdHJpbmcpID0+IEZpcmViYXNlRnVuY3Rpb25zO1xufVxuZXhwb3J0IGRlY2xhcmUgZnVuY3Rpb24gX2ZpcmViYXNlQXBwRmFjdG9yeShvcHRpb25zOiBGaXJlYmFzZU9wdGlvbnMsIG5hbWVPckNvbmZpZz86IHN0cmluZyB8IEZpcmViYXNlQXBwQ29uZmlnIHwgbnVsbCk6IEZpcmViYXNlQXBwO1xuZXhwb3J0IGRlY2xhcmUgY2xhc3MgQW5ndWxhckZpcmVNb2R1bGUge1xuICAgIHN0YXRpYyBpbml0aWFsaXplQXBwKG9wdGlvbnM6IEZpcmViYXNlT3B0aW9ucywgbmFtZU9yQ29uZmlnPzogc3RyaW5nIHwgRmlyZWJhc2VBcHBDb25maWcpOiB7XG4gICAgICAgIG5nTW9kdWxlOiB0eXBlb2YgQW5ndWxhckZpcmVNb2R1bGU7XG4gICAgICAgIHByb3ZpZGVyczoge1xuICAgICAgICAgICAgcHJvdmlkZTogSW5qZWN0aW9uVG9rZW48c3RyaW5nIHwgRmlyZWJhc2VBcHBDb25maWcgfCB1bmRlZmluZWQ+O1xuICAgICAgICAgICAgdXNlVmFsdWU6IHN0cmluZyB8IEZpcmViYXNlQXBwQ29uZmlnIHwgdW5kZWZpbmVkO1xuICAgICAgICB9W107XG4gICAgfTtcbn1cbiJdfQ==