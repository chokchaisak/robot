import { NgZone } from '@angular/core';
import { messaging } from 'firebase/app';
import { Observable } from 'rxjs';
import { FirebaseOptions, FirebaseAppConfig } from '@angular/fire';
import * as ɵngcc0 from '@angular/core';
export declare class AngularFireMessaging {
    messaging: Observable<messaging.Messaging>;
    requestPermission: Observable<void>;
    getToken: Observable<string | null>;
    tokenChanges: Observable<string | null>;
    messages: Observable<{}>;
    requestToken: Observable<string | null>;
    deleteToken: (token: string) => Observable<boolean>;
    constructor(options: FirebaseOptions, nameOrConfig: string | FirebaseAppConfig | null | undefined, platformId: Object, zone: NgZone);
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AngularFireMessaging, [null, { optional: true; }, null, null]>;
    static ɵprov: ɵngcc0.ɵɵInjectableDef<AngularFireMessaging>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVzc2FnaW5nLmQudHMiLCJzb3VyY2VzIjpbIm1lc3NhZ2luZy5kLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBOztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ1pvbmUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IG1lc3NhZ2luZyB9IGZyb20gJ2ZpcmViYXNlL2FwcCc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBGaXJlYmFzZU9wdGlvbnMsIEZpcmViYXNlQXBwQ29uZmlnIH0gZnJvbSAnQGFuZ3VsYXIvZmlyZSc7XG5leHBvcnQgZGVjbGFyZSBjbGFzcyBBbmd1bGFyRmlyZU1lc3NhZ2luZyB7XG4gICAgbWVzc2FnaW5nOiBPYnNlcnZhYmxlPG1lc3NhZ2luZy5NZXNzYWdpbmc+O1xuICAgIHJlcXVlc3RQZXJtaXNzaW9uOiBPYnNlcnZhYmxlPHZvaWQ+O1xuICAgIGdldFRva2VuOiBPYnNlcnZhYmxlPHN0cmluZyB8IG51bGw+O1xuICAgIHRva2VuQ2hhbmdlczogT2JzZXJ2YWJsZTxzdHJpbmcgfCBudWxsPjtcbiAgICBtZXNzYWdlczogT2JzZXJ2YWJsZTx7fT47XG4gICAgcmVxdWVzdFRva2VuOiBPYnNlcnZhYmxlPHN0cmluZyB8IG51bGw+O1xuICAgIGRlbGV0ZVRva2VuOiAodG9rZW46IHN0cmluZykgPT4gT2JzZXJ2YWJsZTxib29sZWFuPjtcbiAgICBjb25zdHJ1Y3RvcihvcHRpb25zOiBGaXJlYmFzZU9wdGlvbnMsIG5hbWVPckNvbmZpZzogc3RyaW5nIHwgRmlyZWJhc2VBcHBDb25maWcgfCBudWxsIHwgdW5kZWZpbmVkLCBwbGF0Zm9ybUlkOiBPYmplY3QsIHpvbmU6IE5nWm9uZSk7XG59XG4iXX0=