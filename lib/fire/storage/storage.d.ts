import { InjectionToken, NgZone } from '@angular/core';
import { AngularFireStorageReference } from './ref';
import { AngularFireUploadTask } from './task';
import { FirebaseStorage, FirebaseOptions, FirebaseAppConfig, FirebaseZoneScheduler } from '@angular/fire';
import { UploadMetadata } from './interfaces';
import * as ɵngcc0 from '@angular/core';
export declare const StorageBucket: InjectionToken<string>;
export declare class AngularFireStorage {
    readonly storage: FirebaseStorage;
    readonly scheduler: FirebaseZoneScheduler;
    constructor(options: FirebaseOptions, nameOrConfig: string | FirebaseAppConfig | null | undefined, storageBucket: string | null, platformId: Object, zone: NgZone);
    ref(path: string): AngularFireStorageReference;
    upload(path: string, data: any, metadata?: UploadMetadata): AngularFireUploadTask;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AngularFireStorage, [null, { optional: true; }, { optional: true; }, null, null]>;
    static ɵprov: ɵngcc0.ɵɵInjectableDef<AngularFireStorage>;
}

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmFnZS5kLnRzIiwic291cmNlcyI6WyJzdG9yYWdlLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3Rpb25Ub2tlbiwgTmdab25lIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBbmd1bGFyRmlyZVN0b3JhZ2VSZWZlcmVuY2UgfSBmcm9tICcuL3JlZic7XG5pbXBvcnQgeyBBbmd1bGFyRmlyZVVwbG9hZFRhc2sgfSBmcm9tICcuL3Rhc2snO1xuaW1wb3J0IHsgRmlyZWJhc2VTdG9yYWdlLCBGaXJlYmFzZU9wdGlvbnMsIEZpcmViYXNlQXBwQ29uZmlnLCBGaXJlYmFzZVpvbmVTY2hlZHVsZXIgfSBmcm9tICdAYW5ndWxhci9maXJlJztcbmltcG9ydCB7IFVwbG9hZE1ldGFkYXRhIH0gZnJvbSAnLi9pbnRlcmZhY2VzJztcbmV4cG9ydCBkZWNsYXJlIGNvbnN0IFN0b3JhZ2VCdWNrZXQ6IEluamVjdGlvblRva2VuPHN0cmluZz47XG5leHBvcnQgZGVjbGFyZSBjbGFzcyBBbmd1bGFyRmlyZVN0b3JhZ2Uge1xuICAgIHJlYWRvbmx5IHN0b3JhZ2U6IEZpcmViYXNlU3RvcmFnZTtcbiAgICByZWFkb25seSBzY2hlZHVsZXI6IEZpcmViYXNlWm9uZVNjaGVkdWxlcjtcbiAgICBjb25zdHJ1Y3RvcihvcHRpb25zOiBGaXJlYmFzZU9wdGlvbnMsIG5hbWVPckNvbmZpZzogc3RyaW5nIHwgRmlyZWJhc2VBcHBDb25maWcgfCBudWxsIHwgdW5kZWZpbmVkLCBzdG9yYWdlQnVja2V0OiBzdHJpbmcgfCBudWxsLCBwbGF0Zm9ybUlkOiBPYmplY3QsIHpvbmU6IE5nWm9uZSk7XG4gICAgcmVmKHBhdGg6IHN0cmluZyk6IEFuZ3VsYXJGaXJlU3RvcmFnZVJlZmVyZW5jZTtcbiAgICB1cGxvYWQocGF0aDogc3RyaW5nLCBkYXRhOiBhbnksIG1ldGFkYXRhPzogVXBsb2FkTWV0YWRhdGEpOiBBbmd1bGFyRmlyZVVwbG9hZFRhc2s7XG59XG4iXX0=