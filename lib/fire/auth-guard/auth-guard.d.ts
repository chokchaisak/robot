import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, UnaryFunction } from 'rxjs';
import { User, auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import * as ɵngcc0 from '@angular/core';
export declare type AuthPipeGenerator = (next: ActivatedRouteSnapshot, state: RouterStateSnapshot) => AuthPipe;
export declare type AuthPipe = UnaryFunction<Observable<User | null>, Observable<boolean | any[]>>;
export declare class AngularFireAuthGuard implements CanActivate {
    private afAuth;
    private router;
    constructor(afAuth: AngularFireAuth, router: Router);
    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree>;
    static ɵfac: ɵngcc0.ɵɵFactoryDef<AngularFireAuthGuard, never>;
    static ɵprov: ɵngcc0.ɵɵInjectableDef<AngularFireAuthGuard>;
}
export declare const canActivate: (pipe: UnaryFunction<Observable<User | null>, Observable<boolean | any[]>> | AuthPipeGenerator) => {
    canActivate: (typeof AngularFireAuthGuard)[];
    data: {
        authGuardPipe: UnaryFunction<Observable<User | null>, Observable<boolean | any[]>> | AuthPipeGenerator | (() => UnaryFunction<Observable<User | null>, Observable<boolean | any[]>> | AuthPipeGenerator);
    };
};
export declare const loggedIn: AuthPipe;
export declare const isNotAnonymous: AuthPipe;
export declare const idTokenResult: import("rxjs").OperatorFunction<User | null, auth.IdTokenResult | null>;
export declare const emailVerified: AuthPipe;
export declare const customClaims: UnaryFunction<Observable<User | null>, Observable<{
    [key: string]: any;
}>>;
export declare const hasCustomClaim: (claim: string) => UnaryFunction<Observable<User | null>, Observable<boolean>>;
export declare const redirectUnauthorizedTo: (redirect: any[]) => UnaryFunction<Observable<User | null>, Observable<true | any[]>>;
export declare const redirectLoggedInTo: (redirect: any[]) => UnaryFunction<Observable<User | null>, Observable<true | any[]>>;

//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1ndWFyZC5kLnRzIiwic291cmNlcyI6WyJhdXRoLWd1YXJkLmQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDYW5BY3RpdmF0ZSwgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgUm91dGVyU3RhdGVTbmFwc2hvdCwgVXJsVHJlZSwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IE9ic2VydmFibGUsIFVuYXJ5RnVuY3Rpb24gfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IFVzZXIsIGF1dGggfSBmcm9tICdmaXJlYmFzZS9hcHAnO1xuaW1wb3J0IHsgQW5ndWxhckZpcmVBdXRoIH0gZnJvbSAnQGFuZ3VsYXIvZmlyZS9hdXRoJztcbmV4cG9ydCBkZWNsYXJlIHR5cGUgQXV0aFBpcGVHZW5lcmF0b3IgPSAobmV4dDogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpID0+IEF1dGhQaXBlO1xuZXhwb3J0IGRlY2xhcmUgdHlwZSBBdXRoUGlwZSA9IFVuYXJ5RnVuY3Rpb248T2JzZXJ2YWJsZTxVc2VyIHwgbnVsbD4sIE9ic2VydmFibGU8Ym9vbGVhbiB8IGFueVtdPj47XG5leHBvcnQgZGVjbGFyZSBjbGFzcyBBbmd1bGFyRmlyZUF1dGhHdWFyZCBpbXBsZW1lbnRzIENhbkFjdGl2YXRlIHtcbiAgICBwcml2YXRlIGFmQXV0aDtcbiAgICBwcml2YXRlIHJvdXRlcjtcbiAgICBjb25zdHJ1Y3RvcihhZkF1dGg6IEFuZ3VsYXJGaXJlQXV0aCwgcm91dGVyOiBSb3V0ZXIpO1xuICAgIGNhbkFjdGl2YXRlKG5leHQ6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIHN0YXRlOiBSb3V0ZXJTdGF0ZVNuYXBzaG90KTogT2JzZXJ2YWJsZTxib29sZWFuIHwgVXJsVHJlZT47XG59XG5leHBvcnQgZGVjbGFyZSBjb25zdCBjYW5BY3RpdmF0ZTogKHBpcGU6IFVuYXJ5RnVuY3Rpb248T2JzZXJ2YWJsZTxVc2VyIHwgbnVsbD4sIE9ic2VydmFibGU8Ym9vbGVhbiB8IGFueVtdPj4gfCBBdXRoUGlwZUdlbmVyYXRvcikgPT4ge1xuICAgIGNhbkFjdGl2YXRlOiAodHlwZW9mIEFuZ3VsYXJGaXJlQXV0aEd1YXJkKVtdO1xuICAgIGRhdGE6IHtcbiAgICAgICAgYXV0aEd1YXJkUGlwZTogVW5hcnlGdW5jdGlvbjxPYnNlcnZhYmxlPFVzZXIgfCBudWxsPiwgT2JzZXJ2YWJsZTxib29sZWFuIHwgYW55W10+PiB8IEF1dGhQaXBlR2VuZXJhdG9yIHwgKCgpID0+IFVuYXJ5RnVuY3Rpb248T2JzZXJ2YWJsZTxVc2VyIHwgbnVsbD4sIE9ic2VydmFibGU8Ym9vbGVhbiB8IGFueVtdPj4gfCBBdXRoUGlwZUdlbmVyYXRvcik7XG4gICAgfTtcbn07XG5leHBvcnQgZGVjbGFyZSBjb25zdCBsb2dnZWRJbjogQXV0aFBpcGU7XG5leHBvcnQgZGVjbGFyZSBjb25zdCBpc05vdEFub255bW91czogQXV0aFBpcGU7XG5leHBvcnQgZGVjbGFyZSBjb25zdCBpZFRva2VuUmVzdWx0OiBpbXBvcnQoXCJyeGpzXCIpLk9wZXJhdG9yRnVuY3Rpb248VXNlciB8IG51bGwsIGF1dGguSWRUb2tlblJlc3VsdCB8IG51bGw+O1xuZXhwb3J0IGRlY2xhcmUgY29uc3QgZW1haWxWZXJpZmllZDogQXV0aFBpcGU7XG5leHBvcnQgZGVjbGFyZSBjb25zdCBjdXN0b21DbGFpbXM6IFVuYXJ5RnVuY3Rpb248T2JzZXJ2YWJsZTxVc2VyIHwgbnVsbD4sIE9ic2VydmFibGU8e1xuICAgIFtrZXk6IHN0cmluZ106IGFueTtcbn0+PjtcbmV4cG9ydCBkZWNsYXJlIGNvbnN0IGhhc0N1c3RvbUNsYWltOiAoY2xhaW06IHN0cmluZykgPT4gVW5hcnlGdW5jdGlvbjxPYnNlcnZhYmxlPFVzZXIgfCBudWxsPiwgT2JzZXJ2YWJsZTxib29sZWFuPj47XG5leHBvcnQgZGVjbGFyZSBjb25zdCByZWRpcmVjdFVuYXV0aG9yaXplZFRvOiAocmVkaXJlY3Q6IGFueVtdKSA9PiBVbmFyeUZ1bmN0aW9uPE9ic2VydmFibGU8VXNlciB8IG51bGw+LCBPYnNlcnZhYmxlPHRydWUgfCBhbnlbXT4+O1xuZXhwb3J0IGRlY2xhcmUgY29uc3QgcmVkaXJlY3RMb2dnZWRJblRvOiAocmVkaXJlY3Q6IGFueVtdKSA9PiBVbmFyeUZ1bmN0aW9uPE9ic2VydmFibGU8VXNlciB8IG51bGw+LCBPYnNlcnZhYmxlPHRydWUgfCBhbnlbXT4+O1xuIl19