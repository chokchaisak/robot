docker rm -f robot-ex-be
docker rmi -f 30010123/robot-ex-be:v1
docker system prune -f
docker build -t 30010123/robot-ex-be:v1 .
docker run -d --name robot-ex-be --restart always -v /root/robot/robotex-backend:/usr/src/app -p 81:81 -p 3000:3000 -p 4444:4444 30010123/robot-ex-be:v1
