const express = require("express");
const app = express();
const cors = require("cors");
const path = require("path");
const fs = require("fs");
const XLSX = require("xlsx");
const multer = require("multer");
const default_path = "alluser";
const socket = require('socket.io');
const { exec } = require("child_process");

app.use(cors());

// app.listen(3000, function () {
//   console.log("listening  port : 3000");
//   //    exec('sh robot-s.sh');
// });

const server = app.listen(3000, () => {
  console.log('Started in 3000');
});

const io = socket(server);

io.sockets.on('connection', (socket) => {
  console.log(`new connection id: ${socket.id}`);
  sendData(socket);
})

function sendData(socket) {
  socket.on('data2', function (user) {
    var spawn = require('child_process').spawn;
      var res = user.testFile.replace(".robot", "");
      var filePath = `./alluser/${user.userId}/PROJECTNAME/LOG/${res}.txt`; 
      try {
         fs.unlinkSync(filePath)
      }
      catch(error){
        console.error(error);
      }
        
      ls = exec(`robot -r ./alluser/${user.userId}/PROJECTNAME/OUTPUT/${res} -o ./alluser/${user.userId}/PROJECTNAME/LOG/output -l ./alluser/${user.userId}/PROJECTNAME/LOG/log ./alluser/${user.userId}/PROJECTNAME/TESTCASE/${user.testFile}`);
      console.log (user)
    ls.stdout.on('data', function (data) {
      console.log('stdout: ' + data);
      fs.appendFile(`./alluser/${user.userId}/PROJECTNAME/LOG/${res}.txt`, data, function (err) {
        if (err) throw err;
        exec(`tail -n+0 ./alluser/${user.userId}/PROJECTNAME/LOG/${res}.txt`, (err, stdout, stderr) => {
          console.log(stdout);
          socket.emit('data2', stdout);
        });
      });

    });

    ls.stderr.on('data', function (data) {
      console.log('stderr: ' + data);
    });

    ls.on('close', function (code) {
      console.log('child process exited with code ' + code);
      exec(`rm ./alluser/${user.userId}/PROJECTNAME/LOG/output.txt`)
    });
  });
}

app.get("/", (req, res) => {
  res.send("from backend");
});

// create user new project
app.get("/users/:userId", (req, res) => {
  const pathNewProject = path.join(__dirname, default_path);
  const directoryPathUser = path.join(pathNewProject, req.params.userId);
  if (!fs.existsSync(directoryPathUser)) {
    fs.mkdirSync(directoryPathUser);
  }
  const directoryPJPath = path.join(directoryPathUser, "PROJECTNAME");
  if (!fs.existsSync(directoryPJPath)) {
    fs.mkdirSync(directoryPJPath);
  }
  const testCaseFolder = path.join(directoryPJPath, "TESTCASE");
  if (!fs.existsSync(testCaseFolder)) {
    fs.mkdirSync(testCaseFolder);
  }
  const testDataFolder = path.join(directoryPJPath, "DATA");
  if (!fs.existsSync(testDataFolder)) {
    fs.mkdirSync(testDataFolder);
  }
  const testoutputFolder = path.join(directoryPJPath, "OUTPUT");
  if (!fs.existsSync(testoutputFolder)) {
    fs.mkdirSync(testoutputFolder);
  }

  return res.send("PROJECTNAME");
});

// Get testcase All file
app.get("/testcase/:userId", (req, res) => {
  const pathNewProject = path.join(__dirname, default_path);
  const directoryPath = path.join(pathNewProject, req.params.userId);
  const directoryPJPath = path.join(directoryPath, "PROJECTNAME");
  const testCaseFolder = path.join(directoryPJPath, "TESTCASE/");
  fs.readdir(testCaseFolder, function (err, file) {
    if (err) {
      return console.log("Unable to scan directory: " + err);
    } else {
      return res.send(file);
    }
  });
  
  // exec("sh summarystop.sh " + req.params.userId, (err, stdout, stderr) => {
  //   if (err) {
  //     console.error(err);
  //     console.log(`stdout: ${stdout}`);
  //     console.log(`stderr: ${stderr}`);
  //     res.status(200).json(stdout);

  //   } else {
  //     console.log(`stdout: ${stdout}`);
  //     console.log(`stderr: ${stderr}`);
  //     res.status(200).json(stdout);

  //   }

  //  });
});

app.get("/testcase/:userId/file/:fileName", (req, res) => {
  const pathNewProject = path.join(__dirname, default_path);
  const directoryPath = path.join(pathNewProject, req.params.userId);
  const directoryPJPath = path.join(directoryPath, "PROJECTNAME");
  const testCaseFolder = path.join(directoryPJPath, "TESTCASE/");
  const Fullpath = testCaseFolder + req.params.fileName;
  const result = fs.readFileSync(Fullpath, "utf-8");
  return res.send(result);
});

// Get data All file
app.get("/data/:userId", (req, res) => {
  const pathNewProject = path.join(__dirname, default_path);
  const directoryPath = path.join(pathNewProject, req.params.userId);
  const directoryPJPath = path.join(directoryPath, "PROJECTNAME");
  const testDataFolder = path.join(directoryPJPath, "DATA");
  fs.readdir(testDataFolder, function (err, file) {
    if (err) {
      return console.log("Unable to scan directory: " + err);
    } else {
      return res.send(file);
    }
  });
});

// Get data All file
app.get("/output/:userId", (req, res) => {
  const pathNewProject = path.join(__dirname, default_path);
  const directoryPath = path.join(pathNewProject, req.params.userId);
  const directoryPJPath = path.join(directoryPath, "PROJECTNAME");
  const testDataFolder = path.join(directoryPJPath, "OUTPUT/");
  fs.readdir(testDataFolder, function (err, file) {
    if (err) {
      return console.log("Unable to scan directory: " + err);
    } else {
      return res.send(file);
    }
  });
});

// Get data All file
app.get("/remove/:userId/type/:Folder/name/:fileName", (req, res) => {
  const pathNewProject = path.join(__dirname, default_path);
  const directoryPath = path.join(pathNewProject, req.params.userId);
  const directoryPJPath = path.join(directoryPath, "PROJECTNAME");
  const testDataFolder = path.join(directoryPJPath, req.params.Folder + "/");
  const Fullpath = testDataFolder + req.params.fileName;
  console.log(Fullpath);
  fs.unlinkSync(Fullpath, function (err) {
    if (err) {
      console.error(err);
    }
    console.log("File has been Deleted");
  });
});
// Get data All file
app.get("/data/:userId/file/:fileName", (req, res) => {
  const pathNewProject = path.join(__dirname, default_path);
  const directoryPath = path.join(pathNewProject, req.params.userId);
  const directoryPJPath = path.join(directoryPath, "PROJECTNAME");
  const testDataFolder = path.join(directoryPJPath, "DATA/");
  const Fullpath = testDataFolder + req.params.fileName;
  const workbook = XLSX.readFile(Fullpath);
  const sheet_name_list = workbook.SheetNames;
  const html = XLSX.utils.sheet_to_html(workbook.Sheets[sheet_name_list[0]], {
    editable: true,
  });
  var cat = html.replace(
    "<table>",
    '<table class="table table-bordered thead-dark">'
  );
  var cat = cat.replace(
    "<tr>",
    '<tr style="background-color: green;color: white;">'
  );
  return res.send(cat);
});

let storageT = multer.diskStorage({
  destination: (req, file, cb) => {
    const pathNewProject = path.join(__dirname, default_path);
    const directoryPath = path.join(pathNewProject, req.params.file);
    const directoryPJPath = path.join(directoryPath, "PROJECTNAME");
    const testDataFolder = path.join(directoryPJPath, "TESTCASE/");

    cb(null, testDataFolder);
  },
  filename: (req, file, cb) => {
    console.log(file.fieldname);
    NameImg = cb(null, file.originalname);
  },
});

let uploadT = multer({ storage: storageT });

app.post("/fileuploadT/:file", uploadT.single("files"), (req, res) => {
  if (!req.file) {
    console.log("No file received");
    return res.send({
      success: false,
    });
  } else {
    console.log("file received");
    return res.send(req.file.filename);
  }
});

let storageE = multer.diskStorage({
  destination: (req, file, cb) => {
    const pathNewProject = path.join(__dirname, default_path);
    const directoryPath = path.join(pathNewProject, req.params.file);
    const directoryPJPath = path.join(directoryPath, "PROJECTNAME");
    const testDataFolder = path.join(directoryPJPath, "DATA/");

    cb(null, testDataFolder);
  },
  filename: (req, file, cb) => {
    console.log(file.fieldname);
    NameImg = cb(null, file.originalname);
  },
});

let uploadE = multer({ storage: storageE });

app.post("/fileuploadE/:file", uploadE.single("files"), (req, res) => {
  if (!req.file) {
    console.log("No file received");
    return res.send({
      success: false,
    });
  } else {
    console.log("file received");
    return res.send(req.file.filename);
  }
});

app.get("/testid/:userId/command/:testFile", (req, res) => {
  const pathNewProject = path.join(__dirname, default_path);
  const directoryPath = path.join(pathNewProject, req.params.userId);
  const directoryPJPath = path.join(directoryPath, "PROJECTNAME");
  // const statment = "robot -T -d "+directoryPJPath+"\\OUTPUT " + directoryPJPath + "\\TESTCASE"
  exec(
    "sh server.sh " + req.params.userId + " " + req.params.testFile,
    (err, stdout, stderr) => {
      if (err) {
        console.error(err);
        console.log(`stdout: ${stdout}`);
        console.log(`stderr: ${stderr}`);
        res.status(200).json(stdout);
      } else {
        console.log(`stdout: ${stdout}`);
        console.log(`stderr: ${stderr}`);
        res.status(200).json(stdout);
      }
    }
  );
});

// Get data All file
app.get("/output/:userId/file/:fileName", (req, res) => {
  const pathNewProject = path.join(__dirname, default_path);
  const directoryPath = path.join(pathNewProject, req.params.userId);
  const directoryPJPath = path.join(directoryPath, "PROJECTNAME");
  const testDataFolder = path.join(directoryPJPath, "OUTPUT/");
  const Fullpath = testDataFolder + req.params.fileName;
  const result = fs.readFileSync(Fullpath, "utf8");
  return res.send(result);
});

// Get data All file
app.get("/view/:userId/file/:fileName", (req, res) => {
  const pathNewProject = path.join(__dirname, default_path);
  const directoryPath = path.join(pathNewProject, req.params.userId);
  const directoryPJPath = path.join(directoryPath, "PROJECTNAME");
  const testDataFolder = path.join(directoryPJPath, "OUTPUT/");
  const Fullpath = testDataFolder + req.params.fileName;
  const result = fs.readFileSync(Fullpath, "utf8");
  return res.send(result);
});

// Get data All file
app.get("/view/:userId/log/:fileName", (req, res) => {
  const pathNewProject = path.join(__dirname, default_path);
  const directoryPath = path.join(pathNewProject, req.params.userId);
  const directoryPJPath = path.join(directoryPath, "PROJECTNAME");
  const testDataFolder = path.join(directoryPJPath, "LOG/");
  const Fullpath = testDataFolder + req.params.fileName;
  const result = fs.readFileSync(Fullpath, "utf8");
  return res.send(result);
});

// Get data All file
app.get("/view/:userId/output/:fileName", (req, res) => {
  const pathNewProject = path.join(__dirname, default_path);
  const directoryPath = path.join(pathNewProject, req.params.userId);
  const directoryPJPath = path.join(directoryPath, "PROJECTNAME");
  const testDataFolder = path.join(directoryPJPath, "OUTPUT/");
  const Fullpath = testDataFolder + req.params.fileName;
  const result = fs.readFileSync(Fullpath, "utf8");
  return res.send(result);
});

app.get("/summary/:userId", (req, res) => {
  const pathNewProject = path.join(__dirname, default_path);
  const directoryPath = path.join(pathNewProject, req.params.userId);
  const directoryPJPath = path.join(directoryPath, "PROJECTNAME");
  console.log(req.params.userId);
  exec("sh summary.sh " + req.params.userId, (err, stdout, stderr) => {
    if (err) {
      console.error(err);
      console.log(`stdout: ${stdout}`);
      console.log(`stderr: ${stderr}`);
      res.status(200).json(stdout);
    } else {
      console.log(`stdout: ${stdout}`);
      console.log(`stderr: ${stderr}`);
      res.status(200).json(stdout);
    }
  });
});

app.get("/summary/:userId/stop", (req, res) => {
  const pathNewProject = path.join(__dirname, default_path);
  const directoryPath = path.join(pathNewProject, req.params.userId);
  const directoryPJPath = path.join(directoryPath, "PROJECTNAME");
  console.log(req.params.userId);
  exec("sh summarystop.sh " + req.params.userId, (err, stdout, stderr) => {
    if (err) {
      console.error(err);
      console.log(`stdout: ${stdout}`);
      console.log(`stderr: ${stderr}`);
      res.status(200).json(stdout);
    } else {
      console.log(`stdout: ${stdout}`);
      console.log(`stderr: ${stderr}`);
      res.status(200).json(stdout);
    }
  });
});

// Get data All file
app.get(
  "/update/:userId/type/:Folder/name/:fileName/content/:conTent",
  (req, res) => {
    const pathNewProject = path.join(__dirname, default_path);
    const directoryPath = path.join(pathNewProject, req.params.userId);
    const directoryPJPath = path.join(directoryPath, "PROJECTNAME");
    const testDataFolder = path.join(directoryPJPath, req.params.Folder + "/");
    const Fullpath = testDataFolder + req.params.fileName;
    const decodedString = req.params.conTent
      .replace(/\`/gi, "\n")
      .replace(/\^/gi, "/");
    fs.writeFile(Fullpath, decodedString, function (err) {
      if (err) {
        return console.log(err);
      }
      console.log("The file was saved!");
    });
  }
);
